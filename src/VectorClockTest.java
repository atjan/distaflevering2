import ddist.Timestamper;
import ddist.util.VectorClock;
import org.junit.*;

public class VectorClockTest {
    private Timestamper a, b;
    @Before
    public void setup(){
        a = new Timestamper("127.0.0.1", 0);
        b = new Timestamper("127.0.0.1", 0);
    }

    @Test
    public void compareEqualVectorClocks() {
        VectorClock first = a.createStamp();
        VectorClock second = b.createStamp();
        Assert.assertEquals("The first two vectors stamps should be equal", 0, first.compareTo(second));
        Assert.assertEquals("The first two vectors stamps should be equal", 0, second.compareTo(first));
    }

    @Test
    public void shouldBeUnequal() {
        VectorClock first = a.createStamp();
        VectorClock second = b.createStamp();

        b.examineStamp(first);
        a.examineStamp(second);

        first = a.createStamp();
        b.examineStamp(first);
        second = b.createStamp();

        Assert.assertEquals("The second timestamp should be smaller", -1, first.compareTo(second));
        Assert.assertEquals("The second timestamp should be greater", 1, second.compareTo(first));
    }
    @Test
    public void shouldEqualize(){
        a.createStamp(); // (1, 0)
        VectorClock second = b.createStamp(); // (0, 1)

        a.examineStamp(second);
        VectorClock first = a.createStamp(); // (2, 1)

        Assert.assertEquals("First should be greater", 1, first.compareTo(second));
        Assert.assertEquals("First should be greater", -1, second.compareTo(first));
        b.examineStamp(first);

        second = b.createStamp(); // (2, 2)
        a.examineStamp(second);

        second = b.createStamp(); // (2, 3)

        first = a.createStamp(); // (3, 2)

        Assert.assertEquals("Be equal again", 0, second.compareTo(first));
        Assert.assertEquals("Be equal again", 0, first.compareTo(second));
    }
}
