package ddist;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Captures all text events, and pushes them into a
 * event queue for later evaluation.
 *
 * This filter is disabled when text actually needs to go into the
 * textfield
 */
public class EventCapturer extends DocumentFilter {
    final private SynchronizedTextArea area;
    private Timestamper ts;

    public EventCapturer(SynchronizedTextArea area) {
        super();
        this.area = area;
        ts = new Timestamper("127.0.0.1", 0);
    }

    public void setStamper(Timestamper ts) {
        this.ts = ts;
    }



    @Override
    public void insertString(FilterBypass db, int off, String str, AttributeSet attr) throws BadLocationException {
        area.pushEvent(new DistributedTextEventInsert(ts, off, str), true);
    }

    @Override
    public void remove(FilterBypass db, int off, int length) throws BadLocationException {
        area.pushEvent(new DistributedTextEventRemove(ts, off, length), true);
    }

    @Override
    public void replace(FilterBypass db, int off, int length, String str, AttributeSet attr) throws BadLocationException {
        if(length != 0) {
            area.pushEvent(new DistributedTextEventReplace(ts, off, length, str), true);
        } else {
            area.pushEvent(new DistributedTextEventInsert(ts, off, str), true);
        }
    }
}

