package ddist;

import ddist.*;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import java.awt.*;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Events are inserted and applied in the right order
 */
public class SynchronizedTextArea extends JTextArea {
    private ArrayList<DistributedTextEvent> events = new ArrayList<DistributedTextEvent>();
    private LinkedBlockingQueue<DistributedTextEvent> ownEvents = new LinkedBlockingQueue<DistributedTextEvent>();

    private EventCapturer capturer = new EventCapturer(this);

    public SynchronizedTextArea(int r, int c){
        super(r, c);
        enableCapture();
    }

    /**
     * We need to make sure the capturing is disabled whenever
     * the text needs to be modified
     */
    private void disableCapture() {
        ((AbstractDocument) getDocument()).setDocumentFilter(null);
    }

    /**
     * Used by server, broadcaster for waiting and sending
     * events out to peers
     * @return new user event
     * @throws InterruptedException
     */
    public DistributedTextEvent takeEvent() throws InterruptedException {
        return ownEvents.take();
    }

    /**
     * Enables capture again, after text content has been modified
     */
    private void enableCapture() {
        ((AbstractDocument) getDocument()).setDocumentFilter(capturer);
    }

    /**
     * Sets the current timestamper, since the timestamper is
     * recreated every time a new is started, the timestamper needs
     * to be updated.
     *
     * @param ts  The new timestamper
     */
    synchronized public void setTimestamper(Timestamper ts) {
        capturer.setStamper(ts);
    }

    /**
     * Used to make sure the text events are in order.
     */
    private void checkOrder() {
        for(int i = 0 ; i < events.size() - 1 ; i ++) {
            if(events.get(i).compareTo(events.get(i  + 1)) != -1) {
                throw new RuntimeException("Not in order!");
            }
        }
    }

    /**
     * Push a new event into the textfield.
     *
     * It checks for several things.
     * First, if the event is local and in order, it is pushed by the IU thread, and can therefore be applied
     * right away.
     *
     * If the event is not in order, it needs to be inserted into the right order,
     * and a event is schedulled to recreate the text content in the textfield, as if all the events
     * were registered in the right order.
     *
     * @param event  The new event to apply
     * @param local If local must be applied directly, so the user does not feel like any
     *              delay in text editing.
     */

    synchronized public void pushEvent(final DistributedTextEvent event, boolean local){
        if(event instanceof DistributedTextEventNull) { // Ignore null events..
            return;
        }


        // Find whether or not the event is in order.
        int pos = -1;
        for(int i = events.size() - 1 ; i >= 0 ; i --) {
            if(event.compareTo(events.get(i)) == -1) {
                pos = i;
            }
        }

        //Local events need to be added to the ownevents queue such that they
        // can be broadcastet by the node.
        if(local){
            ownEvents.add(event);
        }

        // If the pos is -1, then the event is indeed in order.
        if(pos == -1) {

            events.add(event);
            checkOrder();

            if(local) {                     // If local, apply the event directly.
                StringBuilder sb = new StringBuilder(getText());
                event.apply(sb);
                int caret = getCaretPosition();
                setTextNoCapture(sb.toString());

                setCaretPosition(Math.min(event.caretPush(caret), getDocument().getLength()));
                return;
            }

            // If not local, defer the event application whenever the UI thread has time for it.
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    StringBuilder sb = new StringBuilder(getText());
                    event.apply(sb);

                    int caret = getCaretPosition();
                    setTextNoCapture(sb.toString());

                    setCaretPosition(Math.min(getDocument().getLength(), event.caretPush(caret)));
                }
            });
            return;
        }

        // Put the event into order.
        events.add(pos, event);
        checkOrder();
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                // Recreate the text content from scratch.
                StringBuilder sb = new StringBuilder();
                for(int i = 0 ; i < events.size() ; i ++) {
                    events.get(i).apply(sb);
                }
                int position = getCaretPosition();
                position = event.caretPush(position);
                setTextNoCapture(sb.toString());
                setCaretPosition(Math.min(position, getDocument().getLength()));

            }
        });

    }

    /**
     * Utility for just setting the text of the textfield directly
     * used a few places in the push events.
     * @param newText The new text in the textfield
     */
    private void setTextNoCapture(String newText){
        disableCapture();
        setText(newText);
        enableCapture();
    }

    /**
     * Clears the event queue, used whenever a new network is started up
     */
    public void clearEventQueue() {
        events.clear();
        ownEvents.clear();
    }

    /**
     * Returns a snapshot of the current textfield.
     * Used by node to synchronize new peers.
     * @param ts The timestamper to use
     * @return The replace event to broadcast
     */
    public DistributedTextEventFullreplace createSnapShot(Timestamper ts) {
        String txt = getText();
        return new DistributedTextEventFullreplace(ts, txt);
    }

}
