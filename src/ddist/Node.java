package ddist;

import ddist.util.IdIPPortTuple;

import java.io.IOException;
import java.net.*;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;


/**
 * The node handles connected peers
 *
 * A user can choose to create a new node with the listen action
 * or connect to a node (automatially starting their own) with
 * the connectTo action
 */
public class Node {
    /**
     * Returns their this nodes own ID, useful for comparing ids
     * @return Returns this nodes ID
     */
    public IdIPPortTuple getOwnID() {
        return stamper.getOwnID();
    }

    /**
     * The listener task just listens for connections, and spawns peer threads with spawnPeer method
     */
    private class ListenerTask implements Runnable {
        @Override
        public void run() {
            try {
                while(true) {
                    Socket socket = server.accept();
                    spawnPeer(socket);
                }
            } catch(SocketException e) {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Simple utility for keeping the title of the window updated with
     * information about the network.
     *
     * Called whenever events happen that might have changed the number
     * of clients on the network
     */
    private void updateTitle() {
        String ownID = "(peer " + getOwnID().ip + ":" + getOwnID().port + ") ";
        if(peers.size() == 0) {
            editor.setTitle(ownID + "In network with no other peers");
        }
        if(peers.size() == 1) {
            editor.setTitle(ownID + "In network with " + peers.size() + " other peer");

        } else if (peers.size() > 1) {
            editor.setTitle(ownID + "In network with " + peers.size() + " other peers");
        }
    }

    /**
     * The synchronizer task makes sure that all user events are broadcast to the
     * connected peers
     */
    private class SynchronizerTask implements Runnable {
        private int eventCounter = 0;
        @Override
        public void run() {
            updateTitle();
            int counter = 1;

            try {
                while(true) {
                    DistributedTextEvent event = area.takeEvent();
                    counter ++;

                    for(Peer client : peers) {
                        client.pushEvent(event);
                    }
                    updateTitle();
                }
            } catch(InterruptedException e) {
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    private SynchronizedTextArea area;
    private Timestamper stamper;
    private Thread listenerThread;
    private Thread broadcastThread;

    private String ownIP = null;
    private int port = 0;
    private ServerSocket server;
    private HashSet<Peer> peers = new HashSet<Peer>();
    private DistEditor editor;
    private HashSet<IdIPPortTuple> alreadyExamined = new HashSet<>();

    public Node(DistEditor editor, SynchronizedTextArea area) {
        this.area = area;
        this.editor = editor;
    }

    /**
     * Used for fetching the network IP
     * @return The current network IP
     */
    public String getOwnIP() {
        if(this.ownIP != null) return this.ownIP;
        String ownIP = "127.0.0.1";
        Enumeration<NetworkInterface> nets = null;
        try {
            nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface netint : Collections.list(nets)) {
                for(InetAddress addr : Collections.list(netint.getInetAddresses())) {
                    String ip = addr.toString();
                    ip = ip.substring(1);
                    if(ip.split("\\.").length != 4) continue;
                    if(ip.equals("127.0.0.1")) continue;
                    ownIP = ip;
                }
            }
        } catch (SocketException e) {}
        this.ownIP = ownIP;
        return ownIP;
    }

    /**
     * Starts a new network
     * @param port The port to start the network on
     * @return true if network created succesfully
     */
    public boolean startListening(int port){
        if(listenerThread != null) return false;
        if(broadcastThread != null) return false;

        this.port = port;
        alreadyExamined.clear();
        peers.clear();

        area.clearEventQueue();

        String ownIP = getOwnIP();

        try {
            server = new ServerSocket(port);
            stamper = new Timestamper(ownIP, port);
            area.setTimestamper(stamper);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to start server");
            editor.setTitle("Failed to start server (port already in use?)");
            return false;
        }

        listenerThread = new Thread(new ListenerTask());
        listenerThread.start();

        broadcastThread = new Thread(new SynchronizerTask());
        broadcastThread.start();

        System.out.println("Successfully started server");
        return true;
    }

    /**
     * Connects to a peer. Triggered by either the user selecting 'Conect to network' action
     * or when a new id is detected in a timestamp
     * @param ip The host to connect to
     * @param port The port to connect to
     * @return true if the we connected, false otherwise
     */
    public boolean connectTo(String ip, int port) {
        String ownIP = getOwnIP();
        if(ip.equals("127.0.0.1")) { //Make sure we translate the localhost to the network IP
            ip = ownIP;
        }
        // Make sure we don't connect to us selves..
        if(getOwnID().ip.equals(ip) && getOwnID().port == port) return false;


        area.setEnabled(false);                 //Disable input while connecting
        System.out.println("Connecting to peer " + ip +  ":" + port);
        try {
            Socket s = new Socket(ip, port);
            Peer client = new Peer(this, s);
            if(client.start()){
                peers.add(client);
                // Handshake with a nullevent
                client.pushEvent(new DistributedTextEventNull(stamper));
                updateTitle();
            }
            area.setEnabled(true);
            return true;
        } catch (Exception e) {
            System.out.println("Failed to connect to peer");
            area.setEnabled(true);
            return false;
        }
    }

    /**
     * Push an event into the synchronized text area.
     * Examines the event before directing it, to check if any new clients
     * connect if any new clients are found
     * @param event The event to push
     */
    synchronized public void pushEvent(DistributedTextEvent event) {
        stamper.examineStamp(event.getTimestamp());
        area.pushEvent(event, false);

        Set<IdIPPortTuple> ids = new HashSet<>(event.getTimestamp().getIndices().keySet());


        for(IdIPPortTuple id : ids) {
            //Make sure not to connect to the same peer twice
            if(alreadyExamined.contains(id))continue;
            alreadyExamined.add(id);
            if(id.equals(stamper.getOwnID()))continue;
            boolean connectToo = true;
            for(Peer p : peers) {
                // If a peer is still getting their ID. Don't do anything
                if(p.getID() == null) return;
                if(p.getID().equals(id)){
                    connectToo = false;
                    break;
                }
            }
            // If this is an unknown ID, make connection to peer.
            if(connectToo){
                if(connectTo(id.ip, id.port)) {
                    alreadyExamined.add(id);
                    for(Peer p : peers) {
                        // Handshake
                        p.pushEvent(new DistributedTextEventNull(stamper));
                    }
                }
            }
        }
    }

    /**
     * Disconnect a peer, removing them from the peer list, and stopping any threads
     * associated with the peer, and closing the buffers.
     *
     * Called by the peer themselves to cleanly die
     * @param who The peer to disconnect
     */
    synchronized public void disconnect(Peer who) {
        who.stop();
        peers.remove(who);
        updateTitle();
        System.out.println(who.getID() + " disconnected");
    }

    /**
     * Spawns a new peer from a socket, used by the listener thread to
     * create new peers.
     * @param socket  The socket to create a new peer from
     */
    private void spawnPeer(Socket socket) {
        System.out.println("Spawning peer!");
        Peer client = new Peer(this, socket);
        if(client.start()){
            peers.add(client);
            client.pushEvent(new DistributedTextEventNull(stamper));
            if(stamper.getHighestID() == getOwnID().id) {
                client.pushEvent(area.createSnapShot(stamper));
            }
            updateTitle();
        }
    }

    /**
     * Kill the node.
     *
     * Triggered by the user to kill the network.
     */
    synchronized public void killNode() {
        System.out.println("Killing node");


        listenerThread.interrupt();
        broadcastThread.interrupt();


        for(Peer peer : peers) {
            peer.stop();
        }

        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        listenerThread = null;
        broadcastThread = null;
        peers.clear();
        alreadyExamined.clear();
        stamper = null;

        editor.resetTitle();
        System.out.println("Successfully killed node");

    }
}
