package ddist;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.ServerSocket;

public class DistEditor extends JFrame {
    private static final int defaultPort = 40599;

    private SynchronizedTextArea textArea;
    private Node node;

    /**
     * setMode handles the switch between a distributed and a local mode,
     * switching to a distributed mode, the user should not be able to switch to
     * a distributed mode again, and should not be able to change parameters.
     *
     * @param connected
     */
    public void setMode(boolean connected) {
        listenItem.setEnabled(!connected);
        connectItem.setEnabled(!connected);

        ipField.setEnabled(!connected);
        portField.setEnabled(!connected);
        ownPortField.setEnabled(!connected);

        disconnectItem.setEnabled(connected);

    }

    /**
     * Handles the 'Connect to network' action.
     */
    public Action connectionAction = new AbstractAction("Connect to Network") {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            setMode(true);
            int port = Integer.parseInt(portField.getText());
            int ownPort = findFreePort(port);

            String ip = ipField.getText();
            setTitle("Connecting to network");
            System.out.println("Connecting to network");


            if(!node.startListening(ownPort)) {
                textArea.setText("");
                resetTitle();
                setMode(false);
                return;
            }
            node.connectTo(ip, port);
        }
    };
    /**
     * Handles the, start network action, setting up a node
     */
    private Action listenAction = new AbstractAction("Start Network") {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            setMode(true);
            int port = Integer.parseInt(ownPortField.getText());
            setTitle("Starting network on " + port);
            System.out.println("Starting network on " + port);
            if(!node.startListening(port)){
                setMode(false);
            }
        }
    };
    /**
     * Handles disconnecting the node
     */
    private Action disconnectAction = new AbstractAction("Disconnect") {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            node.killNode();
            resetTitle();
            setMode(false);
        }
    };

    /**
     * Finds given a port, returns a free port if this port is in use
     * @param port Start port
     * @return  A free port
     */
    public int findFreePort(int port) {
        while(true) {
            try {
                ServerSocket s = new ServerSocket(port);
                s.close();
                break;
            } catch (IOException e) {
                port += 1;
            }
        };
        return port;
    }

    /**
     * Reset the title back to normal
     */
    public void resetTitle() {
        setTitle("Local mode, not distributed");
    }

    private JMenuItem listenItem     = new JMenuItem(listenAction);
    private JMenuItem disconnectItem = new JMenuItem(disconnectAction);
    private JMenuItem connectItem = new JMenuItem(connectionAction);

    private JTextField ipField = new JTextField();
    private JTextField portField = new JTextField();
    private JTextField ownPortField = new JTextField();


    /**
     * We overwrote reset title to have the title prefixed at all times.
     * @param newTitle
     */
    @Override
    public void setTitle(String newTitle){
        super.setTitle("Distributed Editor: " + newTitle);
    }
    public void clearText(){
        textArea.setText("");
        textArea.clearEventQueue();
    }

    public DistEditor(){
        // Set title to 'Local mode'
        resetTitle();

        // Set up the two main components
        textArea = new SynchronizedTextArea(40, 80);
        node = new Node(this, textArea);

        // Rest is cruft for setting up UI
        setVisible(true);


        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

        mainPanel.add(textArea);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(new JLabel("Network IP: "));
        panel.add(ipField);
        mainPanel.add(panel);

        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(new JLabel("Network port: "));
        panel.add(portField);
        mainPanel.add(panel);
        mainPanel.add(new JSeparator());

        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(new JLabel("Own peer port (If port is taken, a empty port is found): "));
        panel.add(ownPortField);
        mainPanel.add(panel);

        add(mainPanel);

        portField.setText("" + defaultPort);
        ownPortField.setText("" + defaultPort);
        ipField.setText("127.0.0.1");

        JMenuBar bar = new JMenuBar();
        JMenu main = new JMenu("Main");
        main.add(listenItem);
        main.add(connectItem);
        main.add(disconnectItem);
        setMode(false);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        bar.add(main);
        setJMenuBar(bar);
        pack();
    }

    public static void main(String args[]){
        new DistEditor();
    }
}
