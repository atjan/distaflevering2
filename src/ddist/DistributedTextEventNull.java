package ddist;

/**
 * Simple null event, often used for setting up the
 * vectorclock stamper classes in the server
 */
public class DistributedTextEventNull extends DistributedTextEvent {
    public DistributedTextEventNull(Timestamper stamper) {
        super(stamper);
    }
    @Override
    void apply(StringBuilder text) {}

    @Override
    int caretPush(int position) {
        return position;
    }


}
