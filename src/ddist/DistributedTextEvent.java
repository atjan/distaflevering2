package ddist;

import ddist.util.VectorClock;

import java.io.Serializable;

/**
 * Base event for handling text events, implements
 * keeping track of timestamps and comparing events
 * for ordering them later in SynchronizedTextArea
 */
public abstract class DistributedTextEvent implements Comparable<DistributedTextEvent>, Serializable{
    final private VectorClock timeStamp;

    public DistributedTextEvent(Timestamper stamper) {
        timeStamp = stamper.createStamp();
    }

    public VectorClock getTimestamp(){
        return timeStamp;
    }

    /**
     * Apply applies this event to a text buffer, modifying it in place
     * @param currentText The buffer to modify
     */
    abstract void apply(StringBuilder currentText);

    /**
     * Since we are not using the default replace/insert/delete events,
     * each event needs to handle the caret
     * @param position initial position
     * @return position after applying event
     */
    abstract int caretPush(int position);

    /**
     * Compare two text events. The events are compared as follows.
     * First compare vectorsclocks, if the vectorsclocks are not comparable
     * compare the ids
     *
     * if the ids are equal, crash clients, as it means that same event has
     * been sent twice
     * @param event The event to compare
     * @return -1 if lower, 1 is higher
     */
    @Override
    public int compareTo(final DistributedTextEvent event){
        // The distributed events should only always be orderable
        int diff = timeStamp.compareTo(event.timeStamp);
        if(diff != 0) return diff;

        long id = - event.getTimestamp().getID().id;
        if(timeStamp.getID().id > event.getTimestamp().getID().id){
            return 1;
        }
        if(timeStamp.getID().id < event.getTimestamp().getID().id) {
            return -1;
        }

        throw new RuntimeException("Can't compare stamps!");
    }

    @Override
    public String toString() {
        return "DistributedTextEvent{" +
                "timeStamp=" + timeStamp +
                '}';
    }
}
