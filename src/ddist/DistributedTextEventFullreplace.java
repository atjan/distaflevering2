package ddist;

/**
 * Fullreplace event is for creating snapshots of the current textbuffer
 * and replacing other peers text buffer
 */
public class DistributedTextEventFullreplace extends DistributedTextEvent {
    private final String text;
    public DistributedTextEventFullreplace(Timestamper stamper, String text) {
        super(stamper);
        this.text = text;
    }


    @Override
    void apply(StringBuilder currentText) {
        // Set the current textbuffer to nothing,
        // and insert own text
        currentText.setLength(0);
        currentText.append(text);
    }

    @Override
    int caretPush(int position) {
        if(position > text.length()) {
            return text.length();
        }
        return position;
    }
}
