package ddist;

/**
 * Remove event is triggered whenver someone removes/deletes parts of text
 */
public class DistributedTextEventRemove extends DistributedTextEvent {
    final public int offset, length;
    public DistributedTextEventRemove(Timestamper ts, int offset, int length) {
        super(ts);
        this.offset = offset;
        this.length = length;
    }

    @Override
    public void apply(StringBuilder currentText) {
        // Handle offsets / length out of range in some manner
        int start = this.offset;
        int end = this.offset + this.length;

        start = Math.max(start, 0);
        start = Math.min(start, currentText.length());

        end = Math.max(end, 0);
        end = Math.min(end, currentText.length());

        currentText.delete(start, end);
    }

    @Override
    int caretPush(int position) {
        if(position - 1 >= offset) {
            if(position <= offset + length) { // If caret is within selection
                return offset;
            }
            // If caret is to the right of the selection
            return position - length;
        }
        // If the caret is to the left of the selection, don't move it as
        // It is not affected by the text event
        return position;
    }

    @Override
    public String toString() {
        return "DistributedTextEventRemove{" +
                "offset=" + offset +
                ", length=" + length +
                "} " + super.toString();
    }
}
