package ddist.util;

import java.io.Serializable;

/**
 * Simple container to identify a peer
 */
public class IdIPPortTuple implements Serializable{
    final public String ip;
    final public long id;
    final public int port;

    public IdIPPortTuple(String ip, long id, int port) {
        this.ip = ip;
        this.id = id;
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdIPPortTuple that = (IdIPPortTuple) o;

        if (id != that.id) return false;
        if (port != that.port) return false;
        if (!ip.equals(that.ip)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ip.hashCode();
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + port;
        return result;
    }

    @Override
    public String toString() {
        return "IdIPPortTuple{" +
                "ip='" + ip + '\'' +
                ", id=" + id +
                ", port=" + port +
                '}';
    }
}
