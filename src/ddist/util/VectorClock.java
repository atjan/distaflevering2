package ddist.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * ddist.util.VectorClock, used to order all text events across peers
 */
public class VectorClock implements Comparable<VectorClock>, Serializable {
    final private IdIPPortTuple id;
    final private HashMap<IdIPPortTuple, Integer> entries;

    public VectorClock(IdIPPortTuple id, HashMap<IdIPPortTuple, Integer> from) {
        this.id = id;
        entries = new HashMap<IdIPPortTuple, Integer>();
        for(Map.Entry<IdIPPortTuple, Integer> entry : from.entrySet()) {
            entries.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * returns a copy of the timestamp data
     * @return
     */
    public HashMap<IdIPPortTuple, Integer> getIndices() {
        HashMap<IdIPPortTuple, Integer> out = new HashMap<IdIPPortTuple, Integer>();
        for(Map.Entry<IdIPPortTuple, Integer> e : entries.entrySet()) {
            out.put(e.getKey(), e.getValue());
        }
        return out;
    }

    /**
     *
     * @param vectorClock
     * @return 1 if ddist.util.VectorClock is higher, -1 if it is lower, 0 if the two clocks are
     * not comparable, in which case the code should use the ID.
     */
    @Override
    public int compareTo(VectorClock vectorClock) {

        for(IdIPPortTuple key : entries.keySet()) {
            if(vectorClock.entries.containsKey(key)) {
                continue;
            }
            vectorClock.entries.put(key, 0);
        }
        for(IdIPPortTuple key : vectorClock.entries.keySet()) {
            if(entries.containsKey(key)) {
                continue;
            }
            entries.put(key, 0);
        }


        // Check if all are less

        boolean allLEQ = true;
        boolean oneLess = false;
        for(Map.Entry<IdIPPortTuple, Integer> entry : entries.entrySet()) {
            int ownValue = entry.getValue();
            int otherValue = vectorClock.entries.get(entry.getKey());

            if(ownValue < otherValue) {
                oneLess = true;
            }
            if(ownValue > otherValue) {
                allLEQ = false;
            }
        }
        if(allLEQ && oneLess) {
            return -1;
        }

        boolean allGEQ = true;
        boolean oneGreater = false;
        for(Map.Entry<IdIPPortTuple, Integer> entry : entries.entrySet()) {
            int ownValue = entry.getValue();
            int otherValue = vectorClock.entries.get(entry.getKey());

            if(ownValue > otherValue) {
                oneGreater = true;
            }
            if(ownValue < otherValue) {
                allGEQ = false;
            }
        }
        if(oneGreater && allGEQ) {
            return 1;
        }
        return 0;
    }
    public IdIPPortTuple getID(){
        return id;
    }

    @Override
    public String toString() {
        return "ddist.util.VectorClock{" +
                "id=" + id +
                ", entries=" + entries +
                '}';
    }
}
