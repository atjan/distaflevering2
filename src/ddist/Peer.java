package ddist;

import ddist.util.IdIPPortTuple;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Used for handling a peer, listening for events from the peer
 * and sending own events to the peer.
 */
public class Peer {
    private IdIPPortTuple identifier;
    private ObjectInputStream inStream;
    private ObjectOutputStream outStream;

    /**
     * Waits for user textevents to arrive in the 'events' queue
     * redirects them towards the peer.
     */
    private class ClientOutputTask implements Runnable {

        @Override
        public void run() {
            try {
                outStream.writeObject(server.getOwnID());
                while(true) {
                    DistributedTextEvent event = events.take();
                    outStream.writeObject(event);
                }
            } catch(InterruptedException e) {
                disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                disconnect();
            }
        }
    }

    /**
     * Listens for peer events, and redirects them towrads the syncrhonized text
     * area. The area handles any new connects and such.
     */
    private class ClientInputTask implements Runnable {
        @Override
        public void run() {
            try {
                identifier = (IdIPPortTuple)inStream.readObject();      //Start by getting the ID.
                while(true) {
                    DistributedTextEvent event = (DistributedTextEvent)inStream.readObject();
                    server.pushEvent(event);
                }
            } catch(EOFException e) {
                disconnect();
            }catch (Exception e) {
                e.printStackTrace();
                disconnect();
            }

        }
    }

    /**
     * Returns this peers ID, usefull for comparing IDs
     * @return The ID of this peer
     */
    public IdIPPortTuple getID(){
        return identifier;
    }

    /**
     * Disconnects this peer, the disconnected field makes sure this event is only fired once.
     */
    synchronized public void disconnect() {
        if(disconnected) return;
        disconnected = true;
        server.disconnect(this);
    }
    private boolean disconnected = false;
    private final Node server;
    private final Socket socket;
    private Thread out, in;
    private LinkedBlockingQueue<DistributedTextEvent> events = new LinkedBlockingQueue<DistributedTextEvent>();

    public Peer(Node server, Socket socket) {
        this.server = server;
        this.socket = socket;
    }

    /**
     * Used for pushing events into the event queue, such that they
     * are sent to the peer at some point.
     * @param event The event to send.
     */
    public void pushEvent(DistributedTextEvent event) {
        events.add(event);
    }

    /**
     * Starts this peer connection by setting up threads and
     * streams.
     * @return true if everything went well, false if something went wrong
     */
    public boolean start(){
        System.out.println("Start");
        try {
            outStream = new ObjectOutputStream(socket.getOutputStream());
            outStream.flush();
            inStream = new ObjectInputStream(socket.getInputStream());


            out = new Thread(new ClientOutputTask());
            in = new Thread(new ClientInputTask());
            in.start();
            out.start();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Stops this peer, killing input/output threads and streams.
     *
     * Disconnects from the peer.
     */
    public void stop() {
        disconnected = true;

        out.interrupt();
        in.interrupt();

        try {
            socket.close();
            inStream.close();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
