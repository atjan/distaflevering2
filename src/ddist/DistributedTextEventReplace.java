package ddist;

/**
 * Replace event
 */
public class DistributedTextEventReplace extends DistributedTextEvent {
    public final int offset, length;
    public final String txt;

    public DistributedTextEventReplace(Timestamper ts, int offset, int length, String txt) {
        super(ts);
        this.offset = offset;
        this.length = length;
        this.txt = txt;
    }

    /**
     * Applies the event to the textbuffer,
     * does not care if end position
     * is above the real end position.
     * @param currentText The buffer to modify
     */
    @Override
    public void apply(StringBuilder currentText) {
        //System.out.println("replace");
        int start = offset;
        int end = offset + length;

        start = Math.max(start, 0);
        start = Math.min(start, currentText.length());

        end = Math.max(end, 0);
        end = Math.min(end, currentText.length());

        currentText.replace(start, end, txt);
    }

    /**
     * Since we are replacing text with something else, it does not matter
     * where the position is. In most cases handles just fine
     * @param position initial position
     * @return
     */
    @Override
    int caretPush(int position) {
        // > 0 if replacing something smaller with something bigger
        // < 0 if replacing something bigger with something smaller
        int diff = txt.length() - length;

        if(position - 1 >= offset) {

            // Inside replace range
            if(position <= offset + length) {
                return position + Math.max(0, diff);
            }

            // To the right of replace range
            return position + diff;
        }
        // To the left of replace range
        return position;
    }

    @Override
    public String toString() {
        return "DistributedTextEventReplace{" +
                "offset=" + offset +
                ", length=" + length +
                ", txt='" + txt + '\'' +
                "} " + super.toString();
    }
}
