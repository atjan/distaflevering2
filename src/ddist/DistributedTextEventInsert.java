package ddist;

/**
 * Insert event.
 */
public class DistributedTextEventInsert extends DistributedTextEvent {
    public final int offset;
    public final String text;

    public DistributedTextEventInsert(Timestamper ts, final int offset, final String text) {
        super(ts);
        this.offset = offset;
        this.text = text;
    }
    @Override
    public void apply(StringBuilder currentText) {
        // Stay within bounds of the textbuffer.
        int off = Math.max(offset, 0);
        off = Math.min(currentText.length(),off);
        currentText.insert(off, text);
    }

    @Override
    int caretPush(int position) {
        if(offset <= position) {
            return  position + text.length();
        }
        return position;
    }

    @Override
    public String toString() {
        return "DistributedTextEventInsert{" +
                "offset=" + offset +
                ", text='" + text + '\'' +
                "} " + super.toString();
    }
}
