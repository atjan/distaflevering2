package ddist;

import ddist.util.IdIPPortTuple;
import ddist.util.VectorClock;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class for generating new timestamps for the text events
 *
 * The stampers generates a unique ID, and uses this id together
 * with a IP and port to create a unique timestamp
 *
 *
 * The class also handles reading new timestamps, such that the
 * vectorclocks are always as updated as possible.
 *
 */
public class Timestamper {
    private IdIPPortTuple ownID;
    private long ownPort;
    private HashMap<IdIPPortTuple, Integer> time = new HashMap<IdIPPortTuple, Integer>();
    private Set<IdIPPortTuple> knownIds;

    public Timestamper(String ownIP, int ownPort){
        ownID = new IdIPPortTuple(ownIP, 9223372036854775807L - System.currentTimeMillis(), ownPort);
        time.put(ownID, 0);
    }

    synchronized public VectorClock createStamp() {
        time.put(ownID, time.get(ownID) + 1);
        return new VectorClock(ownID, time);
    }



    public void examineStamp(VectorClock c) {
        for(Map.Entry<IdIPPortTuple, Integer> e : c.getIndices().entrySet()) {
            IdIPPortTuple k = e.getKey();
            int v = e.getValue();

            if(!time.containsKey(k)) {
                time.put(k ,v);
            } else {
                int ownV = time.get(k);
                time.put(k, Math.max(v, ownV));
            }

        }
    }

    public long getHighestID() {
        long k = ownID.id;
        for(IdIPPortTuple id : time.keySet()) {
            if(id.id > k){
                k = id.id;
            }
        }
        return k;
    }

    @Override
    public String toString() {
        return "ddist.Timestamper{" +
                "ownID=" + ownID +
                ", time=" + time +
                '}';
    }

    public Set<IdIPPortTuple> getKnownIds() {
        return time.keySet();
    }

    public IdIPPortTuple getOwnID() {
        return ownID;
    }
}
